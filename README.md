# jass-admin

Admin controls for jass website

# Architecture

This is an SPA, with the front end written in Go, compiled to JS via GopherJS.

Covers the following functionality :

- Login
	- Main Menu Actions (displayed as Card set)
		- Users Maint
		- Blog Maint
			- List
			- Add / Upload and scale images
			- Edit / Upload and scale images
			- Delete
		- Shop Category Maint
			- List 
			- Add / Edit / Delete
			- Shop Category Product Maint
				- Product Stock Control
				- List
				- Add / Edit / Delete
				- Report on transactions by product
		- Shipping Types Maint
			- List
			- Add / Edit / Delete
		- Customers Maint
			- List
			- Add / Edit / Delete
			- List Newsletters recv
			- List Orders
				- View Order Details
		- Newsletter Maint
			- List
			- Add / Edit / Delete
			- Generate Mailout
		- Orders Management
			- List
			- Add / Edit / Delete
		- Reports
			- Outgoing Links
			- Referrers
			- Sales, graph by category and product and region


